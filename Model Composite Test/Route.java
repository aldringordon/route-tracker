/**
 * Aldrin Gordon
 */
import java.util.*;
public class Route implements Segment
{
    private String name;
    private String desc;
    private WayPoint start;
    private WayPoint finish;
    private ArrayList<WayPoint> wayPoints;

    public Route(String name, String desc)
    {
        this.name = name;
        this.desc = desc;
        this.start = null;
        this.finish = null;
        this.wayPoints = new ArrayList<>();
    }

    @Override public WayPoint find(String desc)
    {
        for(Segment waypoint : wayPoints)
        {
            WayPoint found = waypoint.find(desc);
            if(found != null)
            {
                return found;
            }
        }
        return null;
    }

    @Override public int countWayPoints()
    {
        int size = 0;
        for(Segment wayPoint : wayPoints)
        {
            size += wayPoint.countWayPoints();
        }
        return size;
    }

    @Override public int countSubRoutes()
    {
        int size = 0;
        for(Segment wayPoint : wayPoints)
        {
            size += wayPoint.countSubRoutes();
        }
        return size;
    }

    public void addWayPoint(WayPoint wayPoint)
    {
        if(this.countWayPoints() == 0) // start waypoint
        {
            wayPoints.add(wayPoint);
            this.start = wayPoints.get(0);
        }
        else if(wayPoint.getDesc() == null) // no desc = finish waypoint
        {
            wayPoints.add(wayPoint);
            this.finish = wayPoints.get(wayPoints.size() - 1);
        }
        else
        {
            wayPoints.add(wayPoint);
        }
    }

    public String getName()
    {
        return this.name;
    }

    public ArrayList<WayPoint> getWayPoints()
    {
        return this.wayPoints;
    }

    public String toString()
    {
        return new String(this.name + "," + this.desc + ", Start: " + this.start + " End: " + this.finish);
    }

    public boolean isEqual(Route route)
    {
        if(this.toString().equals(route.toString()))
        {
            return true;
        }
        return false;
    }
}
