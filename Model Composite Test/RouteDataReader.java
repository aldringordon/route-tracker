/**
 * Aldrin Gordon
 * reads route data information returned from
 *  GeoUtils.retrieveRouteData()
 * and passes each line to RouteFactory to create
 * each Route and WayPoint.
 * Returns a HashMap of Routes once String has been fully parsed.
 */
import java.util.*;

public class RouteDataReader
{
    public RouteDataReader() { /* hi */}

    public HashMap<String, Route> readData(String data)
    {
        HashMap<String, Route> routes = new HashMap<>();
        String[] lineData = data.split("\n", -1); // split string into individual lines
        String currRoute = null;

        for(int i = 0; i < lineData.length; i++)
        {
            String line = lineData[i];
            if(!line.equals("") && !line.contains(",")) // line isnt empty, and has no commas = route line
            {
                Route route = makeRoute(line);
                currRoute = route.getName();
                routes.put(currRoute, route);
            }
            else if(!line.equals("") && line.contains(",")) // line isnt empty, and has commas = waypoint line
            {
                WayPoint wayPoint = makeWayPoint(line);
                routes.get(currRoute).addWayPoint(wayPoint);
            } // else empty line = do nothing
        }
        return routes;
    }

    private Route makeRoute(String data)
    {
        String[] lineData = data.split(" ", -1); // split route line on space
        String name = lineData[0];
        String desc = data.replace(name, "");

        Route route = new Route(name, desc);
        return route;
    }

    private WayPoint makeWayPoint(String data)
    {
        String[] lineData = data.split(",", -1); // split waypoint line on comma
        String desc = null;
        double lat = Double.parseDouble(lineData[0]);
        double lon = Double.parseDouble(lineData[1]);
        double alt = Double.parseDouble(lineData[2]);

        if(lineData.length > 3) // every waypoint line that isnt the finish waypoint
        {
            String clearStr = new String(lineData[0] + "," + lineData[1] + "," + lineData[2] + ",");
            desc = data.replace(clearStr, "");
        } // finish waypoint desc stays null

        WayPoint wayPoint = new WayPoint(desc, lat, lon, alt);
        return wayPoint;
    }
}
