public class SegmentException extends Exception
{
    public SegmentException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
