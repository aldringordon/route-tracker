/**
 * Aldrin Gordon
 * test Views
 */
import java.io.*;
import java.util.*;
public class Test
{
    public static void main(String[] args)
    {
        String data = getTestString();
        RouteDataReader reader = new RouteDataReader();
        HashMap<String, Route> routes = reader.readData(data);

        MainView mainView;
        mainView = new MainView();
        mainView.showMenu();
        mainView.showRoutes(routes);

        RouteView routeView = new RouteView();
        routeView.showMenu();
        routeView.showRouteInfo(routes, routes.get("mainRoute"));
    }

    public static String getTestString()
    {
        return new String("\ntheClimb Amazing views!\n    -31.94,115.75,47.1,Easy start\n    -31.94,115.75,55.3,Tricky, watch for drop bears.\n    -31.94,115.75,71.0,I*feel,like.over-punctuating!@#$%^&*()[]{}<>.?_+\n    -31.93,115.75,108.0,Getting there\n    -31.93,115.75,131.9\n\nmainRoute Since I was young\n    -31.96,115.80,63.0,I knew\n    -31.95,115.78,45.3,I'd find you\n    -31.95,115.77,44.8,*theStroll\n    -31.94,115.75,47.1,But our love\n    -31.93,115.72,40.1,Was a song\n    -31.94,115.75,47.1,*theClimb\n    -31.93,115.75,131.9,Sung by a dying swan\n    -31.92,115.74,128.1\n\ntheStroll Breathe in the light\n    -31.95,115.77,44.8,I'll stay here\n    -31.93,115.76,43.0,In the shadow\n    -31.94,115.75,47.1\n\n");
    }
}
