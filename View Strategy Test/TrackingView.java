/**
 * Aldrin Gordon
 * view for "tracking" mode/state
 */
public class TrackingView implements AppView
{
    @Override public void showMenu()
    {
        System.out.println("\tTRACKING MENU");
        System.out.println("1) - toggle waypoint");
        System.out.println("2) - return");
    }

    @Override public String getInput()
    {
        Scanner sc = new Scanner(System.in);
        System.out.print("> ");
        return sc.nextLine();
    }

    public void showLocation(WayPoint currLoc, WayPoint currWay, int distance)
    {
        System.out.println("Current Location: " + currLoc.getCoords());
        System.out.println("Current WayPoint: " + currWay.getDesc()); // can assume != null because reaching last waypoint terminates
        System.out.println("Distance Left: " + distance + "meters");
    }
}
