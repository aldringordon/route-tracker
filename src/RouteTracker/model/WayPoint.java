package routetracker.model;
public class WayPoint implements Segment
{
    private String desc;
    private double lat;
    private double lon;
    private double alt;

    public WayPoint()
    {
        this.desc = null;
        this.lat = -1;
        this.lon = -1;
        this.alt = -1;
    }

    public WayPoint(String desc, double lat, double lon, double alt)
    {
        this.desc = desc;
        this.lat = lat;
        this.lon = lon;
        this.alt = alt;
    }

    @Override public WayPoint find(String desc)
    {
        WayPoint found = null;
        if(this.desc.equals(desc))
        {
            found = this;
        }
        return found;
    }

    @Override public int countWayPoints()
    {
        return 1;
    }

    @Override public int countSubRoutes()
    {
        if(checkSubRoute() != null)
        {
            return 1;
        }
        return 0;
    }

    public double getLat()
    {
        return this.lat;
    }

    public double getLon()
    {
        return this.lon;
    }

    public double getAlt()
    {
        return this.alt;
    }

    public String getDesc()
    {
        return this.desc;
    }

    public String getCoords()
    {
        return new String(lat + "," + lon + "," + alt);
    }

    public void updateLocation(double lat, double lon, double alt)
    {
        this.lat = lat;
        this.lon = lon;
        this.alt = alt;
    }

    public String checkSubRoute()
    {
        String routeName = null;
        if(this.desc != null && this.desc.startsWith("*"))
        {
            routeName = this.desc.replace("*", "");
        }
        return routeName;
    }

    public String toString()
    {
        return new String(this.lat + "," + this.lon + "," + this.alt + " : \"" + this.desc + "\"");
    }

    public boolean isEqual(WayPoint wayPoint)
    {
        if(this.toString().equals(wayPoint.toString()))
        {
            return true;
        }
        return false;
    }
}
