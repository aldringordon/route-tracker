/**
 * Aldrin Gordon
 * view for main menu i/o
 * outputs basic route information
 */
package routetracker.view;
import java.io.*;
import java.util.*;
public class MainView implements AppView
{
    @Override public void showMenu()
    {
        System.out.println("\tMAIN MENU");
        System.out.println("1) - download routes");
        System.out.println("2) - select route");
        System.out.println("3) - exit");
    }

    @Override public String getInput()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("> ");
        return sc.nextLine();
    }

    public void showRoute(String name, String startCoords, String finishCoords, double horizontal, double climb, double descent)
    {
        System.out.println("Route: " + name + ".");
        System.out.println("    Start: "+ startCoords + ".");
        System.out.println("    End: " + finishCoords + ".");
        System.out.println("    Distance:");
        System.out.println("        - Horizontal: "+ horizontal + " metres.");
        System.out.println("        - Vertical Climbing: " + climb + " metres.");
        System.out.println("        - Vertical Descent: " + descent + " metres.");
    }
}
