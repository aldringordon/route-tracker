/**
 * Aldrin Gordon
 * view for route information
 */
package routetracker.view;
import java.io.*;
import java.util.*;
import routetracker.model.*; // yeah not good
public class RouteView implements AppView
{
    @Override public void showMenu()
    {
        System.out.println("\tROUTE MENU");
        System.out.println("1) - go");
        System.out.println("2) - return");
    }

    @Override public String getInput()
    {
        Scanner sc = new Scanner(System.in);
        System.out.print("> ");
        return sc.nextLine();
    }

    public void showAskRoute()
    {
        System.out.println("- Input Route Name");
    }

    public void routeError(String input)
    {
        System.out.println("Cannot find route: " + input);
    }

    public void showRouteInfo(HashMap<String, Route> routes, Route route)
    {
        System.out.print("\nMAIN ROUTE: ");
        showSegments(routes, route);
    }

    /**
     * outputs route information for a specified
     * route and its subroutes
     *      - description
     *      - waypoints
     *      - segments
     *          - coordinates
     *          - descriptions
     * @param routes [description]
     * @param route  [description]
     */
    private void showSegments(HashMap<String, Route> routes, Route route)
    {
        System.out.println(route.getName());
        System.out.println("Description:" + route.getDesc());
        System.out.println("Segments:");
        for(WayPoint wayPoint : route.getWayPoints())
        {
            if(wayPoint.getDesc() != null && wayPoint.getDesc().startsWith("*")) // indicates subroute
            {
                String subRoute = wayPoint.getDesc().replace("*", ""); // subroute name, ommit *
                System.out.print("\nSTART OF SUBROUTE: ");
                showSegments(routes, routes.get(subRoute)); // recursive call to print subroute information
            }
            else
            {
                System.out.println(wayPoint.getCoords());
                if(wayPoint.getDesc() != null)
                {
                    System.out.println("\t" + wayPoint.getDesc());
                }
                else
                {
                    System.out.println("\t\tEND OF ROUTE: " + route.getName() + "\n");
                }

            }
        }
    }
}
