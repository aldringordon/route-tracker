/**
 * Aldrin Gordon
 * view interface
 */
package routetracker.view;
public interface AppView
{
    public void showMenu();
    public String getInput();
}
