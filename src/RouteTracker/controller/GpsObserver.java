/**
 * Aldrin Gordon
 * observer pattern interface for subject class: GpsReader
 */
package routetracker.controller;
import routetracker.model.WayPoint;
public interface GpsObserver
{
    protected GpsReader reader;
    protected TrackingMode context;
    public void locationUpdated(WayPoint wayPoint);
}
