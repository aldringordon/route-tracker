/**
 * Aldrin Gordon
 * contains methods related to "tracking" mode
 */
package routetracker.controller;
import routetracker.model.*;
import routetracker.view.TrackingView;
public class TrackingMode
{
    private TrackingView view;
    private DistanceUtils distUtils; // calculate distances uses GeoUtils
    private ArrayList<WayPoint> segments; // all waypoints in the route (including subroutes) in travel order
    private ArrayList<WayPoint> path;

    // route totals (includes subroutes)
    private double overallDistance;
    private double overallClimbing;
    private double overallDescent;

    private WayPoint currentWayPoint; // start of current segment

    // distance information of TRAVELLED segments
    private double totalSegmentDistance;
    private double totalSegmentClimb;
    private double totalSegmentDescent;

    private WayPoint currentLocation; // location received from GpsLocator
    private double distanceTravelled; // totalSegmentDistance + distance(currWayPoint -> location)
    private double distanceClimbed; // totalSegmentClimb + if POSITIVE = vertDifference(currWayPoint -> location)
    private double distanceDescended; // totalSegmentDescent + ABS VAL if NEGATIVE = vertDifference(currWayPoint -> location)

    public TrackingMode(TrackingView view, DistanceUtils distUtils, ArrayList<WayPoint> segments, double overallDistance, double overallClimbing, double overallDescent, WayPoint currentWayPoint)
    {
        this.view = view;
        this.distUtils = distUtils;
        this.segments = segments;
        this.path = new ArrayList<>();
        this.overallDistance = overallDistance;
        this.overallClimbing = overallClimbing;
        this.overallDescent = overallDescent;
        this.currentWayPoint = currentWayPoint;

        this.totalSegmentDistance = 0;
        this.totaleSegmentClimb = 0;
        this.totalSegmentDescent = 0;

        this.currentLocation = null;
        this.distanceTravelled = 0;
        this.distanceClimbed = 0;
        this.distanceDescended = 0;
    }

    public ArrayList<WayPoint> go() // start tracking mode
    {
        int option;
        this.path.clear(); // initialize path taken to empty

        // starts updating location on new thread
        GpsReader gps = new GpsReader();

        // when GpsReader receives a new location from GpsLocator
        // GpsReader notifies the TrackingOberserver which calls
        // this class updateLocation() method
        TrackingObserver obs = new TrackingObserver(gps, this);

        view.showMenu();
        view.showWayPoint(currentWayPoint.getCoords());

        do
        {
            try
            {
                option = Integer.parseInt(view.getInput()); // 1 = toggle waypoint, 2 = return
            }
            catch(Exception e)
            {
                view.showMenu();
                option = 0;
            }

            if(option == 1) //toggle next waypoint
            {
                updatePath();
            }
        }while(option != 2 && currentWayPoint.desc != null);

        return path;
    }

    /**
     * method called from TrackingObserver controlled by
     * GpsLocator on a different thread.
     * when called, method updates:
     *      - users current location;
     *      - the distance to the next waypoint;
     *      - distanceLeft in the route;
     *      - currentWayPoint if within range;
     *      - updates path taken by adding currentWayPoint to path list;
     * @param location WayPoint location retrieved from GpsLocator.locationReceived()
     */
    public void updateLocation(WayPoint location)
    {
        currentLocation = location; // update location
        updateDistanceTravelled(); // update distance left

        // check if within range of next waypoint
        if(distUtils.withinRange(currentLocation, nextWayPoint))
        {
            updatePath(); // update path travelled and set next way point
            view.showWayPoint(currentWayPoint.getCoords()); // show current waypoint
        }

        // output update information to user
        view.showlocation(currentLocation.getCoords(), overallDistance - distanceTravelled, overallClimbing - distanceClimbed, overallDescent - distanceDescended));
    }

    /**
     * called when nextWayPoint has been reached either
     * when currentLocation is withinRange of nextWayPoint
     * or when user manually enters toggle nextWayPoint by
     * pressing 2 (in go())
     *
     * updates the total of the segments travelled by adding
     * distance of the completed segment, then updates currentWayPoint
     * to nextWayPoint to start new segment
     */
    private void updatePath()
    {
        Distance segDistance = distUtils.calculateDistance(currentWayPoint, nextWayPoint);
        path.add(currentWayPoint);
        totalSegmentDistance += segDistance.getHorizontal();
        if(segDistance.getVertical() > 0) // climbed
        {
            totalSegmentClimb += segDistance.getVertical();
        }
        else
        {
            totalSegmentDescent += Math.abs(segDistance.getVertical());
        }
        setNextWayPoint(); // set currentWayPoint to nextWayPoint
    }

    /**
     * update distance travelled = total distance of finished segment + distance travelled in current segment
     */
    private void updateDistanceTravelled()
    {
        WayPoint nextWayPoint =  getNextWayPoint(currentWayPoint);// to calculate distance
        Distance segTravelled = distUtils.calculateDistance(currentWayPoint, currentLocation); // segment travelled

        distanceTravelled = totalSegmentDistance + segTravelled.getHorizontal();
        if(segTravelled.getVertical() > 0)
        {
            distanceClimbed = totalSegmentClimb + segTravelled.getVertical();
        }
        else
        {
            distanceDescended = totalSegmentDescent + Math.abs(segTravelled.getVertical());
        }

    }

    /**
     * get the next waypoint and set as current
     */
    private void setNextWayPoint()
    {
        currentWayPoint = getNextWayPoint();
    }

    /**
     * return the next waypoint after current
     */
    private WayPoint getNextWayPoint()
    {
        int index = segments.indexOf(currentWayPoint);
        try
        {
            return segments.get(index + 1);
        }
        catch(IndexOutOfBoundsException e)
        {
            return null;
        }
    }
}
