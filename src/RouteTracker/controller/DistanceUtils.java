/**
 * Aldrin Gordon
 * methods to:
 *      - calculate distances for each segment
 *      - calculate total route distance
 *      - calculate distance between 2 waypoints
 */
package routetracker.controller;
import java.util.*;
import routetracker.model.*;
public class DistanceUtils
{
    private GeoUtils geoUtils;

    public DistanceUtils(GeoUtils geoUtils)
    {
        this.geoUtils = geoUtils;
    }

    /**
    * calculates the distance between each segment and updates
    * each waypoint distance
    * @param segments list of segments in route to update
     * @return          total route distance, summation of segment lengths
     */
    public double calculateSegmentsHorizontal(ArrayList<WayPoint> segments)
    {
        double totalDistance, segmentDistance;
        WayPoint currentWayPoint, nextWayPoint;
        int currentIndex, nextIndex;
        totalDistance = 0;
        try
        {
            for(nextIndex = 1; nextIndex < segments.size(); nextIndex++) // start at 2nd waypoint
            {
                currentIndex = nextIndex - 1; // index of waypoint before
                currentWayPoint = segments.get(currentIndex);
                nextWayPoint = segments.get(nextIndex);

                segmentDistance = calculateHorizontalDistance(currentWayPoint, nextWayPoint);
                totalDistance += segmentDistance;
            }
        }
        catch(IndexOutOfBoundsException e)
        {
            totalDistance = -1;
        }

        return totalDistance;
    }

    /**
    * calculates the vertical distance between each segment and updates
    * climb if vertical difference is positive
    * @param segments list of segments in route to update
     * @return          total route distance, summation of segment lengths
     */
    public double calculateSegmentsClimb(ArrayList<WayPoint> segments)
    {
        double totalClimb, segmentClimb;
        WayPoint currentWayPoint, nextWayPoint;
        int currentIndex, nextIndex;
        totalClimb = 0;
        try
        {
            for(nextIndex = 1; nextIndex < segments.size(); nextIndex++) // start at 2nd waypoint
            {
                currentIndex = nextIndex - 1; // index of waypoint before
                currentWayPoint = segments.get(currentIndex);
                nextWayPoint = segments.get(nextIndex);

                segmentClimb = calculateVerticalDistance(currentWayPoint, nextWayPoint);
                if(segmentClimb > 0)
                {
                    totalClimb += segmentClimb;
                }
            }
        }
        catch(IndexOutOfBoundsException e)
        {
            totalClimb = -1;
        }

        return totalClimb;
    }

    /**
    * similair to calculateSegmentsClimb but for descent
    * @param segments list of segments in route to update
     * @return          total route distance, summation of segment lengths
     */
    public double calculateSegmentsDescent(ArrayList<WayPoint> segments)
    {
        double totalDescent, segmentDescent;
        WayPoint currentWayPoint, nextWayPoint;
        int currentIndex, nextIndex;
        totalDescent = 0;
        try
        {
            for(nextIndex = 1; nextIndex < segments.size(); nextIndex++) // start at 2nd waypoint
            {
                currentIndex = nextIndex - 1; // index of waypoint before
                currentWayPoint = segments.get(currentIndex);
                nextWayPoint = segments.get(nextIndex);

                segmentDescent = calculateVerticalDistance(currentWayPoint, nextWayPoint);
                if(segmentDescent > 0)
                {
                    totalDescent += segmentDescent;
                }
            }
        }
        catch(IndexOutOfBoundsException e)
        {
            totalDescent = -1;
        }

        return totalDescent;
    }

    /**
     * calls GeoUtils.calcMetresDistance() to calculate
     * the distance in metres between two waypoints
     * @param  wayPoint1
     * @param  wayPoint2
     * @return           distance in metres
     */
    public double calculateHorizontalDistance(WayPoint wayPoint1, WayPoint wayPoint 2)
    {
        double lat1, lon1, lat2, lon2, metres;

        lat1 = currentLocation.getLat();
        lon1 = currentLocation.getLon();
        lat2 = nextWayPoint.getLat();
        lon2 = nextWayPoint.getLon();

        metres = geoUtils.calcMetresDistance(lat1, lon1, lat2, lon2);
        return metres;
    }

    public double calculateVerticalDistance(WayPoint wayPoint1, WayPoint wayPoint2)
    {
        double alt1, alt2;
        alt1 = wayPoint1.getAlt();
        alt2 = wayPoint2.getAlt();
        return alt2 - alt1;
    }

    /**
     * calculate horizontal & vertical distance between 2 waypoints
     * @param  wayPoint1 [description]
     * @param  wayPoint2 [description]
     * @return           [description]
     */
    public Distance calculateDistance(WayPoint wayPoint1, WayPoint wayPoint2)
    {
        double horizontal = calculateHorizontalDistance(wayPoint1, wayPoint2);
        double vertical = calculateVerticalDistance(wayPoint1, wayPoint2);
        Distance distance = new Distance(horizontal, vertical);
        return distance;
    }

    /**
     * check if current location is within range of destination
     * @param  current current location
     * @param  dest    destination location
     * @return         true if horizontal < 10 AND veritcal < 2
     */
    public boolean withinRange(WayPoint current, WayPoint dest)
    {
        boolean inRange = false;
        Distance distance = calculateDistance(current, dest);
        if(distance.getHorizontal() < 10 && Math.abs(distance.getVertical()) < 2) // abs value for within 2m range up or down
        {
            inRange = true;
        }
        return inRange;
    }

    public ArrayList<WayPoint> getSegments(HashMap<String, Route> routes, Route route)
    {
        ArrayList<WayPoint> wayPoints = new ArrayList<>();
        for(WayPoint wayPoint : route.getWayPoints())
        {
            if(wayPoint.getDesc() != null && wayPoint.getDesc().startsWith("*")) // indicates subroute
            {
                String subRoute = wayPoint.getDesc().replace("*", ""); // subroute name, ommit *
                wayPoints.addAll(getSegments(routes, routes.get(subRoute))); // recursive call to get subroute information and appen to list
            }
            else
            {
                wayPoints.add(wayPoint);
            }
        }
        return wayPoints;
    }
}
