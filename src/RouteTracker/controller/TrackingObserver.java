/**
 * Aldrin Gordon
 * concrete observer
 */
package routetracker.controller;
import routetracker.model.WayPoint;
public class TrackingObserver extends GpsObserver
{
    public TrackingObserver(GpsReader reader, TrackingMode context)
    {
        this.context = context;
        this.reader = reader;
        this.reader.addObserver(this);
    }

    @Override public void locationUpdated(WayPoint wayPoint)
    {
        context.updateLocation(wayPoint);
    }
}
