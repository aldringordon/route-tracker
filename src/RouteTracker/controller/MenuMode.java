/**
 * Aldrin Gordon
 * contains methods related to the default app mode
 */
package routetracker.controller;
import java.util.*;
import routetracker.model.*;
import routetracker.view.MainView;
import routetracker.view.RouteView;
public class MenuMode
{
    private GeoUtils geoUtils;
    private DistanceUtils distUtils;
    private MainView mainView;
    private RouteView routeView;

    public MenuMode(GeoUtils geoUtils, DistUtils distUtils, MainView mainView, RouteView routeView)
    {
        this.geoUtils = geoUtils;
        this.distUtils = distUtils;
        this.routeView = routeView;
        this.mainView = mainView;
    }

    public int run(HashMap<String, Route> routes)
    {
        int option;
        boolean done = true;

        showTable(routes);
        mainView.showMenu();
        do
        {
            try
            {
                option = Integer.parseInt(mainView.getInput());
                if(option == 1 || option == 2 || option == 3)
                {
                    done = false;
                }
            }
            catch(ParseException e)
            {
                mainView.showMenu();
            }
        }while(done);

        return option;
    }

    public ArrayList<WayPoint> selectRoute(HashMap<String, Route> routes)
    {
        boolean done = true;
        int option;
        Route chosenRoute;
        String routeName;
        ArrayList<WayPoint> segments;

        do
        {
            try
            {
                routeView.showAskRoute();
                routeName = routeView.getInput();
                chosenRoute = routes.get(routeName);
                if(chosenRoute == null)
                {
                    throw new IllegalArgumentException();
                }

                routeView.showMenu();
                option = Integer.parseInt(routeView.getInput());
                if(option == 1) // go
                {
                    segments = distUtils.getSegments(routes, chosenRoute);
                    done = false;
                }
                else if(option == 2) // return
                {
                    done = false;
                    segments = null;
                }
            }
            catch(ParseException e)
            {
                routeView.showMenu();
            }
            catch(IOException e)
            {
                showTable(routes);
                routeView.showMenu();
            }
            catch(IllegalArgumentException e)
            {
                showTable(routes);
                routeView.showMenu();
                routeView.routeError(routeName);
            }
        }while(done);
        return segments;
    }

    /**
     * output route information
     * @param routes [description]
     */
    private void showTable(HashMap<String, Route> routes)
    {
        String name, startCoords, finishCoords;
        double horizontal, climb, descent;
        for(Route route : routes.values())
        {
            name = route.getName();
            startCoords = route.getStart().getCoords();
            finishCoords = route.getFinish().getCoords();
            horizontal = distUtils.calculateSegmentsHorizontal(route.getWayPoints());
            climb = distUtils.calculateSegmentsClimb(route.getWayPoints());
            descent = distUtils.calculateSegmentsDescent(route.getWayPoints());

            view.showRoute(name, startCoords, finishCoords, horizontal, climb, descent);
        }
    }

    public String download()
    {
        try
        {
            return geoUtils.retrieveRouteData();
        }
        catch(IOException e)
        {
            return "routeData_download_failed";
        }
    }

    public double getHorizontal(ArrayList<WayPoint> segments)
    {
        return distUtils.calculateSegmentsHorizontal(segments);
    }

    public double getClimb(ArrayList<WayPoint> segments)
    {
        return distUtils.calculateSegmentsClimb(segments);
    }

    public double getDescent(ArrayList<WayPoint> segments)
    {
        return distUtils.calculateSegmentsDescent(segments);
    }
}
