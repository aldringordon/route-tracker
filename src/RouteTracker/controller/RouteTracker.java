import java.util.*;
import routetracker.controller.*;
import routetracker.view.*;
import routetracker.model.*;

/**
 * Aldrin Gordon
 * RouteTracker.java
 * contains main()
 */

public class RouteTracker
{
    public static void main(String[] args)
    {
        int option;
        double distance, climb, descent;
        ArrayList<WayPoint> segments;
        TrackingMode trackingMode;

        GeoUtils geoUtils = new geoUtils();
        DistUtils distUtils = new DistUtils();
        MainView mainView = new MainView();
        RouteView routeView = new RouteView();
        TrackingView trackingView = new TrackingView();
        RouteDataReader dataReader = new RouteDataReader();

        MenuMode menu = new MenuMode(geoUtils, distUtils, mainView, routeView);
        String routeData = menu.download();
        HashMap<String, Route> routes = dataReader.readData(routeData);

        do
        {
            option = menu.run();
            if(option == 1)
            {
                routeData = menu.download();
                routes = dataReader.readData(routeData);
            }
            else if(option == 2)
            {
                segments = menu.selectRoute(routes);
                if(segments != null) // user entered valid route name
                {
                    distance = menu.getHorizontal(segments);
                    climb = menu.getClimb(segments);
                    descent = menu.getDescent(segments);
                    trackingMode = new TrackingMode(trackingView, distUtils, segments, distance, climb, descent, segments.get(0));
                    trackingMode.go();
                }
            }
        }while(option != 3);
    }
}
