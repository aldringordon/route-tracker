/**
 * Aldrin Gordon
 */

public interface Segment
{
    WayPoint find(String desc);
    int countWayPoints();
    int countSubRoutes();
}
