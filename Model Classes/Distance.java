/**
 * Aldrin Gordon
 * distance class holds distance information
 * horizontal and vertical
 */
public class Distance
{
    private double horizontal;
    private double vertical;

    public Distance(double horizontal, double vertical)
    {
        this.horizontal = horizontal;
        this.vertical = vertical;
    }

    public double getHoriztonal()
    {
        return this.horizontal;
    }

    public void setHorizontal(double horizontal)
    {
        this.horizontal = horizontal;
    }

    public double getVeritcal()
    {
        return this.vertical;
    }

    public void setVertical(double vertical)
    {
        this.vertical = vertical;
    }

    public String toString()
    {
        return new String("Distance = Horizontal: " + horizontal + " metres, Vertical: " + vertical + " metres");
    }
}
