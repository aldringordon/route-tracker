/**
 * Aldrin Gordon
 * observer pattern interface for subject class: GpsReader
 */
public interface GpsObserver
{
    protected GpsReader reader;
    protected TrackingMode context;
    public void locationUpdated(WayPoint wayPoint);
}
