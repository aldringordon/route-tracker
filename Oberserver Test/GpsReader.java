/**
 * Aldrin Gordon
 * Template Method GpsLocator concrete Class implementation
 */
import java.util.*;
public class GpsReader extends GpsLocator
{
    private WayPoint currentLocation = new WayPoint();
    private ArrayList<GpsObserver> obs = new ArrayList<>();

    @Override protected void locationReceived(double latitude, double longitude, double altitude)
    {
        currentLocation.updateLocation(latitude, longtitude, altitude);
        updateLocation();
    }

    public void addObserver(GpsObserver ob)
    {
        obs.add(ob);
    }

    public void updateLocation()
    {
        for(GpsObserver ob : obs)
        {
            obs.locationUpdated(currentLocation);
        }
    }
}
