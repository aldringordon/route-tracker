/**
 * Aldrin Gordon
 * concrete observer
 */
public class TrackingObserver extends GpsObserver
{
    public TrackingObserver(GpsReader reader, TrackingMode context)
    {
        this.context = context;
        this.reader = reader;
        this.reader.addObserver(this);
    }

    @Override public void locationUpdated(WayPoint wayPoint)
    {
        context.updateLocation(wayPoint);
    }
}
