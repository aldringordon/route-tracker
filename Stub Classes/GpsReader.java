/**
 * Aldrin Gordon
 * Template Method GpsLocator concrete Class implementation
 */
public class GpsReader extends GpsLocator
{
    WayPoint currentLocation;

    public GpsReader(TrackingView view)
    {
        this.currentLocation = new WayPoint();
    }

    @Override protected void locationReceived(double latitude, double longitude, double altitude)
    {
        this.currentLocation.updateLocation(latitude, longtitude, altitude);
    }
}
