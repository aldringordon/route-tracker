/**
 * GpsLocator abstract class for Template Method
 * based off appendix
 * unkown template method
 * test one implemented
 */
public abstract class GpsLocator
{
    /**
    * When GpsLocator has received a new set of coordinates, it calls
    * this hook method.
    */
    protected abstract void locationReceived(double latitude, double longitude, double altitude);

    /**
     * Constructing GpsLocator sets up a new thread and calls
     * the hook method locationReceived() every 2 seconds
     * with a latitude, longtitude and altitude
     *
     */

    public void templateMethod()
    {
        // create new thread
        // call locationReceived every 2 seconds
        // when user exits or location = waypoint -> exit
        // restart locator when next section is selected
    }
}
