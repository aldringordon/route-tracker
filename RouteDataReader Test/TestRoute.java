/**
 * Container Class to test RouteDataReader
 * holds name, description, start and finish
 * uses TestWaypoint to store waypoint information
 */
import java.io.*;
import java.util.*;

public class TestRoute
{
    public String name;
    public String desc;
    public ArrayList<TestWayPoint> waypoints;

    public TestRoute(String name, String desc)
    {
        this.name = name;
        this.desc = desc;
        this.waypoints = new ArrayList<>();
    }

    public void addWayPoint(String inLat, String inLon, String inAlt)
    {
        TestWayPoint inWayPoint;
        Double lat, lon, alt;

        /* parse String values to Double */
        lat = Double.parseDouble(inLat);
        lon = Double.parseDouble(inLon);
        alt = Double.parseDouble(inAlt);

        /* create waypoint object */
        inWayPoint = new TestWayPoint(lat, lon, alt);

        /*add waypoint object to list */
        waypoints.add(inWayPoint);
    }

    public TestWayPoint getStart()
    {
        return waypoints.get(0);
    }

    public TestWayPoint getFinish()
    {
        return waypoints.get(waypoints.size() - 1);
    }
}
