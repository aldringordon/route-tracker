/**
 * Container Class to test RouteDataReader
 * holds WayPoint information:
 * latitude
 * longtitude
 * altitutde
 */
public class TestWayPoint
{
    public double lat;
    public double lon;
    public double alt;

    public TestWayPoint(double lat, double lon, double alt)
    {
        this.lat = lat;
        this.lon = lon;
        this.alt = alt;
    }
}
