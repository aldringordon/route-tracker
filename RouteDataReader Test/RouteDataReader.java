import java.io.*;
import java.util.*;
/**
 * Reads / Formats String information
 */
public class RouteDataReader
{
    public HashMap<String, TestRoute> routes;

    public RouteDataReader()
    {
        this.routes = new HashMap<>();
    }

    public void readData(String data)
    {
        String routeName = null;
        String routeDesc = null;
        String wayPointDesc = null;
        String[] lines = data.split("\n", -1);
        String[] currLine;

        for(int i = 0; i<lines.length; i++)
        {
            if(lines[i].equals("")) // check if empty line
            {
                System.out.println("\n" + i + ". EMPTY LINE\n");
            }
            else if(lines[i].contains(",")) // check if waypoint line
            {
                String subStr;
                currLine = lines[i].split(",", -1);
                routes.get(routeName).addWayPoint(currLine[0], currLine[1], currLine[2]);

                if(currLine.length > 3)
                {
                    subStr = new String(currLine[0] + "," + currLine[1] + "," + currLine[2] + ",");
                    wayPointDesc = lines[i].replace(subStr, "");
                }
                else
                {
                    wayPointDesc = "END OF ROUTE";
                }

                System.out.println(i + ". Waypoint: " + Double.parseDouble(currLine[0]) + ", " + Double.parseDouble(currLine[1]) + ", " + Double.parseDouble(currLine[2]) + " Desc: " + wayPointDesc);
            }
            else // if neither, means start of new route
            {
                TestRoute route;
                currLine = lines[i].split(" ", -1);
                routeName = currLine[0];
                routeDesc = lines[i].replace(routeName, ""); //remove route name from stirng, left with description

                route = new TestRoute(routeName, routeDesc);
                routes.put(routeName, route);

                System.out.println(i + ". \n\tRoute: " + routeName + "\n\tDescription: " + routeDesc);
            }
        }
/* TEST

        for(int i = 0; i<lines.length; i++)
        {
            System.out.println(i + ". " + lines[i]);
        }
*/

    }

    public HashMap<String, TestRoute> getRoutes()
    {
        return routes;
    }
}
