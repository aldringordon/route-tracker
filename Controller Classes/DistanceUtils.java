/**
 * Aldrin Gordon
 * methods to:
 *      - calculate distances for each segment
 *      - calculate total route distance
 *      - calculate distance between 2 waypoints
 */
import java.util.*;

public class DistanceUtils
{
    private GeoUtils geoUtils;

    public DistanceUtils(GeoUtils geoUtils)
    {
        this.geoUtils = geoUtils;
    }

    /**
    * calculates the distance between each segment and updates
    * each waypoint distance
    * @param segments list of segments in route to update
     * @return          total route distance, summation of segment lengths
     */
    public Distance calculateSegmentDistances(ArrayList<WayPoint> segments)
    {
        Distance totalDistance;
        WayPoint currentWayPoint, nextWayPoint;
        int currentIndex, nextIndex;
        double segmentHDistance, segmentVDistance, totalHDistance, totalVDistance;
        totalHDistance = 0;
        totalVDistance = 0;
        try
        {
            for(nextIndex = 1; i < segments.size(); i++) // start at 2nd waypoint
            {
                currentIndex = nextIndex - 1; // index of waypoint before
                currentWayPoint = segments.get(currentIndex);
                nextWayPoint = segments.get(nextIndex);

                segmentHDistance = calculateHorizontalDistance(currentWayPoint, nextWayPoint);
                segmentVDistance = calculateVerticalDistance(currentWayPoint, nextWayPoint);
                currentWayPoint.setDistance(segmentHDistance, segmentVDistance);
                totalHDistance += segmentHDistance;
                totalVDistance += segmentVDistance;
            }
            totalDistance = new Distance(totalHDistance, totalVDistance);
        }
        catch(IndexOutOfBoundsException e)
        {
            totalDistance = null;
        }

        return totalDistance;
    }

    /**
     * calls GeoUtils.calcMetresDistance() to calculate
     * the distance in metres between two waypoints
     * @param  wayPoint1
     * @param  wayPoint2
     * @return           distance in metres
     */
    public double calculateHorizontalDistance(WayPoint wayPoint1, WayPoint wayPoint 2)
    {
        double lat1, lon1, lat2, lon2, metres;

        lat1 = currentLocation.getLat();
        lon1 = currentLocation.getLon();
        lat2 = nextWayPoint.getLat();
        lon2 = nextWayPoint.getLon();

        metres = geoUtils.calcMetresDistance(lat1, lon1, lat2, lon2);
        return metres;
    }

    public double calculateVerticalDistance(WayPoint wayPoint1, WayPoint wayPoint2)
    {
        double alt1, alt2;
        alt1 = wayPoint1.getAlt();
        alt2 = wayPoint2.getAlt();
        return alt2 - alt1;
    }

    /**
     * calculate horizontal & vertical distance between 2 waypoints
     * @param  wayPoint1 [description]
     * @param  wayPoint2 [description]
     * @return           [description]
     */
    public Distance calculateDistance(WayPoint wayPoint1, WayPoint wayPoint2)
    {
        double horizontal = calculateHorizontalDistance(wayPoint1, wayPoint2);
        double vertical = calculateVerticalDistance(wayPoint1, wayPoint2);
        Distance distance = new Distance(horizontal, vertical);
        return distance;
    }

    /**
     * check if current location is within range of destination
     * @param  current current location
     * @param  dest    destination location
     * @return         true if horizontal < 10 AND veritcal < 2
     */
    public boolean withinRange(WayPoint current, WayPoint dest)
    {
        boolean inRange = false;
        Distance distance = calculateDistance(current, dest);
        if(distance.getHorizontal() < 10 && Math.abs(distance.getVertical()) < 2) // abs value for within 2m range up or down
        {
            inRange = true;
        }
        return inRange;
    }
}
