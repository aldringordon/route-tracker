/**
 * Appendix Specific Class GeoUtils
 *
 * WORKING TEST CLASS
 *
 */
public class GeoUtils
{
    /**
    * Returns the horizontal distance (across the Earth's surface) in
    * metres between two points expressed in degrees of latitude and
    * longitude.
    *
    * (If any arguments are out of range, this submodule will erase your
    * hard drive.)
    */
    public double calcMetresDistance(double lat1, double long1, double lat2, double long2)
    {
        // based off appendix approximation calculation
        return 6371000 * Math.acos(Math.sin((Math.PI*lat1)/180) * Math.sin((Math.PI*lat2)/180) + Math.cos((Math.PI*lat1)/180) * Math.cos((Math.PI*lat2)/180) * Math.cos((Math.PI*Math.abs(long1-long2))/180));
    }

    /**
    * Attempts to contact the central server to retrieve the latest version
    * of the route data.
    * @return A string containing formatted route information.
    * @throws IOException if the server cannot be contacted, or the
    * connection is interrupted.
    */
    public String retrieveRouteData() throws IOException
    {
        // string copied from appendix with \n included for new lines
        return new String("\ntheClimb Amazing views!\n    -31.94,115.75,47.1,Easy start\n    -31.94,115.75,55.3,Tricky, watch for drop bears.\n    -31.94,115.75,71.0,I*feel,like.over-punctuating!@#$%^&*()[]{}<>.?_+\n    -31.93,115.75,108.0,Getting there\n    -31.93,115.75,131.9\n\nmainRoute Since I was young\n    -31.96,115.80,63.0,I knew\n    -31.95,115.78,45.3,I'd find you\n    -31.95,115.77,44.8,*theStroll\n    -31.94,115.75,47.1,But our love\n    -31.93,115.72,40.1,Was a song\n    -31.94,115.75,47.1,*theClimb\n    -31.93,115.75,131.9,Sung by a dying swan\n    -31.92,115.74,128.1\n\ntheStroll Breathe in the light\n    -31.95,115.77,44.8,I'll stay here\n    -31.93,115.76,43.0,In the shadow\n    -31.94,115.75,47.1\n\n");
    }
}
