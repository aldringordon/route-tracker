/**
 * Appendix Specific Class GeoUtils
 *
 * NOT WORKING TEST CLASS
 * throws errors
 *
 */
public class GeoUtils
{
    /**
    * Returns the horizontal distance (across the Earth's surface) in
    * metres between two points expressed in degrees of latitude and
    * longitude.
    *
    * (If any arguments are out of range, this submodule will erase your
    * hard drive.)
    */
    public double calcMetresDistance(double lat1, double long1, double lat2, double long2)
    {
        return 0;
    }

    /**
    * Attempts to contact the central server to retrieve the latest version
    * of the route data.
    * @return A string containing formatted route information.
    * @throws IOException if the server cannot be contacted, or the
    * connection is interrupted.
    */
    public String retrieveRouteData() throws IOException
    {
        throw new IOException("error");
        return null;
    }
}
