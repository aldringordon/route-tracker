/**
 * Aldrin Gordon
 * view interface
 */
public interface AppView
{
    public void showMenu();
    public String getInput();
}
