/**
 * Aldrin Gordon
 * view for "tracking" mode/state
 */
public class TrackingView implements AppView
{
    @Override public void showMenu()
    {
        System.out.println("\tTRACKING MENU");
        System.out.println("1) - toggle waypoint");
        System.out.println("2) - return");
    }

    @Override public String getInput()
    {
        Scanner sc = new Scanner(System.in);
        System.out.print("> ");
        return sc.nextLine();
    }

    public void showWayPoint(String wayPoint)
    {
        System.out.println("Current Waypoint: " + wayPoint);
    }

    /**
     * output distance information
     * @param location   current location
     * @param horizontal remainig horizontal distance
     * @param climbing   remaining climb distance
     * @param descent    remaining descent distance
     */
    public void showLocation(String location, double horizontal, double climbing, double descent)
    {
        System.out.println("Current Location: " + Location);
        System.out.println("Remaining Distance:");
        System.out.println("    - Horizontal: " + horizontal + " metres");
        System.out.println("    - Climbing: " + climbing + " metres");
        System.out.println("    - Descent: " + descent + " metres");
    }
}
