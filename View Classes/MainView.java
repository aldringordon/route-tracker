/**
 * Aldrin Gordon
 * view for main menu i/o
 * outputs basic route information
 */
import java.io.*;
import java.util.*;
public class MainView implements AppView
{
    @Override public void showMenu()
    {
        System.out.println("\tMAIN MENU");
        System.out.println("1) - download routes");
        System.out.println("2) - select route");
        System.out.println("3) - exit");
    }

    @Override public String getInput()
    {
        Scanner sc = new Scanner(System.in);
        System.out.print("> ");
        return sc.nextLine();
    }

    public void showRoutes(HashMap<String, Route> routes)
    {
        for(Route route : routes.values())
        {
            System.out.println("Route: " + route.getName());
            System.out.println("\tStart: "+ route.getStart().getCoords());
            System.out.println("\tEnd: " + route.getFinish().getCoords());
            System.out.println("\tDistance: " + route.getDistance());
        }
    }
}
